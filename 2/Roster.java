import java.util.List;
import java.util.ArrayList;

public class Roster
{
    private List<Week> weeks;

    public Roster ()
    {
        weeks = new ArrayList<Week> ();
    }

    public List<Week> getWeeks ()
    {
        return weeks;
    }

    public boolean add (Week wk)
    {
        return weeks.add (wk);
    }

    public String toString ()
    {
        return weeks.toString ();
    }
}
