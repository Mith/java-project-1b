import java.util.Set;
import java.util.TreeSet;

public class Couple
{
    private TreeSet<String> couple;

    public Couple (String s1, String s2)
    {
        couple = new TreeSet<String> ();
        couple.add (s1);
        couple.add (s2);
    }

    public TreeSet<String> getCouple ()
    {
        return couple;
    }

    public String toString ()
    {
        return "(" + couple.first () + " + " + couple.last () + ")";
    }
}
