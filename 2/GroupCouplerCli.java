import java.util.*;
import java.io.*;

public class GroupCouplerCli
{
    public GroupCouplerCli ()
    {
        GroupCoupler coupler = new GroupCoupler (new File ("klas.txt"));
        coupler.setWeeks (25);
        prettyPrint (coupler.calculateCouples ());
    }

    private void prettyPrint (Roster roster)
    {
        int num = 1;

        for (Week week : roster.getWeeks ()) {
            System.out.println ("-- Week " + num + " --");
            num++;
            prettyPrint (week);
        }
    }

    private void prettyPrint (Week week)
    {
        for (Couple couple : week.getCouples ()) {
            System.out.println (couple);
        }
    }

    public static void main (String[] args)
    {
        GroupCouplerCli cli = new GroupCouplerCli ();
    }
}
