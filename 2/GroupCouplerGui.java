import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.io.*;

public class GroupCouplerGui extends JFrame
{
    private GroupCoupler coupler;

    private JScrollPane groupList;

    public GroupCouplerGui ()
    {
        groupList = new JScrollPane ();
        JButton readBtn = new JButton ("Lees in");
        JButton coupleBtn = new JButton ("Produceer koppels");
        JButton checkBtn = new JButton ("Check koppels");
        JPanel buttons = new JPanel ();
        buttons.add (readBtn);
        buttons.add (coupleBtn);
        buttons.add (checkBtn);
        JPanel mainPane = new JPanel ();
        mainPane.setLayout (new BoxLayout (mainPane, BoxLayout.PAGE_AXIS));
        mainPane.add (groupList);
        mainPane.add (buttons);
        pack ();
        setVisible (true);
    }

    public static void main (String[] args)
    {
        GroupCouplerGui gui = new GroupCouplerGui ();
    }

    private class Control implements ActionListener
    {
        public void actionPerformed (ActionEvent e)
        {
        }
    }
}
