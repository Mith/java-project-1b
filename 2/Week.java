import java.util.List;
import java.util.ArrayList;

public class Week
{
    private List<Couple> couples;

    public Week ()
    {
        couples = new ArrayList<Couple> ();
    }

    public Week (List<Couple> cps)
    {
        couples = cps;
    }

    public List<Couple> getCouples ()
    {
        return couples;
    }

    public boolean add (Couple cp)
    {
        return couples.add (cp);
    }

    public String toString ()
    {
        return couples.toString ();
    }
}
