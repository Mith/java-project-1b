import java.io.*;

public class TwoThirteenFinder
{
    public static void main (String[] args)
    {
        String str = "2013";
        int firstChar = findString (loadPi (), str);

        if (firstChar == -1) {
            System.out.println (str + " not found");
        } else {
            System.out.println (str + " found at " + firstChar);
        }
    }

    public static String loadPi ()
    {
        String pi = null;

        try {
            BufferedReader reader = new BufferedReader (new FileReader ("pidigits.txt"));
            pi = reader.readLine ().substring (2);
        } catch (IOException e) {
            e.printStackTrace ();
        }

        return pi;
    }


    // Returns first character position, -1 if none found
    public static int findString (String pi, String str)
    {
        for (int i = 0; i < pi.length (); i++) {
            if (pi.charAt (i) == str.charAt (0)) {
                if (pi.length () > i + str.length ()
                        && pi.substring (i, i + str.length ()).equals (str)) {
                    return i;
                }
            }
        }

        return -1;
    }
}