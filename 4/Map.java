import java.awt.Point;
import java.util.ArrayList;

public class Map
{
    int[][] map;

    public Map (int[][] m)
    {
        map = m;
    }

    public int getMapValue (Point p)
    {
        return getMapValue (p.y, p.y);
    }

    public int getMapValue (int x, int y)
    {
        return map[y][x];
    }

    public int getMapHeight ()
    {
        return map.length;
    }

    public int getMapWidth (int n)
    {
        return map[n].length;
    }

    public boolean isAccessible (Point p)
    {
        return isAccessible (p.x, p.y);
    }

    public boolean isAccessible (int x, int y)
    {
        if (x >= 0 && x < getMapWidth (x) &&
                y >= 0 && y < getMapHeight ()) {
            int v = getMapValue (x, y);
            return v == 0;
        }

        return false;
    }

    public static Point getNeighbor (Point p, Direction dir)
    {
        return getNeighbor (p.x, p.y, dir);
    }

    public static Point getNeighbor (int x, int y, Direction dir)
    {
        switch (dir) {
        case North:
            return new Point (x, y - 1);

        case East:
            return new Point (x + 1, y);

        case South:
            return new Point (x, y + 1);

        case West:
            return new Point (x - 1, y);

        default:
            return null;
        }
    }
}
