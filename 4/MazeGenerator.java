import java.util.*;

public class MazeGenerator
{
    private int height;
    private int width;
    public MazeGenerator (int height, int width)
    {
        this.height = height;
        this.width = width;
        correctSize();
    }
    private void correctSize()
    {
        if (height % 2 == 0) {
            height++;
        }

        if (width % 2 == 0) {
            width++;
        }
    }
    public int [][] generate()
    {
        int [][] maze = new int[height][width];

        //muren vullen
        for (int i = 0; i < width; i++) {
            maze[0][i] = 1;
            maze[height - 1][i] = 1;
        }

        for (int i = 0; i < height; i++) {
            maze[i][0] = 1;
            maze[i][width - 1] = 1;
        }

        //opslaan hoeveel muren er kunnen worden neergezet
        int horWall = (height - 3) / 2;
        int verWall = (width - 3) / 2;
        //mogelijkheden om een muur neer te zetten op horizontale lijn
        List <Integer> posibilitiesHor = new ArrayList<Integer>();

        for (int i = 0; i < horWall; i++) {
            posibilitiesHor.add ( (i + 1) * 2);
        }

        //hoeveel gaten moeten er in de muur en waar
        List <List> horRooms = new ArrayList<List>();
        List <Integer> posibilitiesHorGate = new ArrayList<Integer>();

        for (int i = 1; i <= (width - 1) / 2; i++) {
            posibilitiesHorGate.add ( (i * 2) - 1);
        }

        horRooms.add (posibilitiesHorGate);
        //mogelijkheden om een muur neer te zetten op horizontale lijn
        List <Integer> posibilitiesVer = new ArrayList<Integer>();

        for (int i = 0; i < verWall; i++) {
            posibilitiesVer.add ( (i + 1) * 2);
        }

        //hoeveel gaten moeten er in de muur en waar
        List <List> verRooms = new ArrayList<List>();
        List <Integer> posibilitiesVerGate = new ArrayList<Integer>();

        for (int i = 1; i <= (height - 1) / 2; i++) {
            posibilitiesVerGate.add ( (i * 2) - 1);
        }

        verRooms.add (posibilitiesVerGate);

        //een loop zolang er muren neergezet kunnen worden
        while (posibilitiesHor.size() > 0 || posibilitiesVer.size() > 0) {
            //horizontale muren
            if (posibilitiesHor.size() > 0) {
                //plek zoeken waar een horizontale muur moet komen
                int wallNr = (int) (Math.random() * posibilitiesHor.size());
                int wall = (posibilitiesHor.get (wallNr));
                posibilitiesHor.remove (wallNr);

                for (int i = 0; i < width; i++) {
                    maze[wall][i] = 1;
                }

                //pas de mogelijkheden aan waar een gat in de muur kan komen
                for (List room: verRooms) {
                    if (room.contains (wall - 1) && room.contains (wall + 1)) {
                        List <Integer> newList = new ArrayList<Integer>();

                        for (int i = room.indexOf (wall + 1); i < room.size(); i++) {
                            newList.add ( (Integer) room.get (i));
                        }

                        for (Integer i: newList) {
                            room.remove (i);
                        }

                        verRooms.add (newList);
                        break;
                    }
                }

                //maak een gaatje in de muur voor waar tussen alle verticale muren
                for (List room: horRooms) {
                    int gate = (int) (Math.random() * room.size());
                    gate = (Integer) (room.get (gate));
                    maze[wall][gate] = 0;
                }
            }

            //verticale muren
            if (posibilitiesVer.size() > 0) {
                //verticale muur zoeken en plaatsen
                int wallNr = (int) (Math.random() * posibilitiesVer.size());
                int wall = (posibilitiesVer.get (wallNr));
                posibilitiesVer.remove (wallNr);

                for (int i = 0; i < height; i++) {
                    maze[i][wall] = 1;
                }

                //pas de mogelijkheden aan waar een gat in de muur kan komen
                for (List room: horRooms) {
                    if (room.contains (wall - 1) && room.contains (wall + 1)) {
                        List <Integer> newList = new ArrayList<Integer>();

                        for (int i = room.indexOf (wall + 1); i < room.size(); i++) {
                            newList.add ( (Integer) room.get (i));
                        }

                        for (Integer i: newList) {
                            room.remove (i);
                        }

                        horRooms.add (newList);
                        break;
                    }
                }

                //maak een gaatje in de muur tussen alle Horizontale muren
                for (List room: verRooms) {
                    int gate = (int) (Math.random() * room.size());
                    gate = (Integer) (room.get (gate));
                    maze[gate][wall] = 0;
                }
            }
        }

        return maze;
    }
}
