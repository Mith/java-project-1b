import java.awt.Point;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Comparator;
import java.util.HashSet;

public class PathFinder
{
    private Map map;

    public PathFinder (Map m)
    {
        map = m;
    }

    public ArrayList<Point> findPath (final Point a, final Point b)
    {
        PriorityQueue<Node> open = new PriorityQueue<Node> (10, new Comparator<Node> () {
            public int compare (Node n1, Node n2) {
                Integer f1 = n1.getCostFromStart (a) + distance (n1.getCoordinates (), b);
                int f2 = n2.getCostFromStart (a) + distance (n2.getCoordinates (), b);
                return f1.compareTo (f2);
            }
        });
        HashSet<Node> closed = new HashSet<Node> ();
        open.add (new Node (a));

        while (open.size () != 0) {
            Node curNode = open.poll ();

            if (closed.contains (curNode)) {
                continue;
            }

            if (curNode.getCoordinates ().x == b.x &&
                    curNode.getCoordinates ().y == b.y) {
                System.out.println ("Found path");
                return nodeToPath (curNode);
            }

            closed.add (curNode);

            for (Node n : curNode.getNeighbors ()) {
                if (map.isAccessible (n.getCoordinates ())) {
                    int cost = curNode.getCostFromStart (a) + 1;
                    n.setCostFromStart (cost);
                    open.add (n);
                } else {
                    closed.add (n);
                }
            }
        }

        return null;
    }

    private ArrayList<Point> nodeToPath (Node n)
    {
        ArrayList<Point> p = new ArrayList<Point> ();
        return nodeToPath (n, p);
    }

    private ArrayList<Point> nodeToPath (Node n, ArrayList<Point> p)
    {
        if (n.getParent () == null) {
            p.add (n.getCoordinates ());
            return p;
        } else {
            p.add (n.getCoordinates ());
            return nodeToPath (n.getParent (), p);
        }
    }

    private int distance (Point a, int x, int y)
    {
        return Math.abs (a.x - x) + Math.abs (a.y - y);
    }

    private int distance (Point a, Point b)
    {
        return distance (a, b.x, b.y);
    }

    class Node
    {
        private Point coordinates;
        private Node parent;
        private int costFromStart = -1;

        public Node (Point p)
        {
            coordinates = p;
        }

        public Node (Node p, Point c)
        {
            coordinates = c;
            parent = p;
        }

        public Node (Node p, int x, int y)
        {
            coordinates = new Point (x, y);
            parent = p;
        }

        public Node getParent ()
        {
            return parent;
        }

        public void setParent (Node p)
        {
            parent = p;
        }

        public Point getCoordinates ()
        {
            return coordinates;
        }

        public int getCostFromStart (Point s)
        {
            if (costFromStart == -1) {
                costFromStart = distance (coordinates, s);
            }

            return costFromStart;
        }

        public void setCostFromStart (int nc)
        {
            costFromStart = nc;
        }

        public ArrayList<Node> getNeighbors ()
        {
            ArrayList<Node> ns = new ArrayList<Node> ();
            ns.add (new Node (this, map.getNeighbor (coordinates, Direction.North)));
            ns.add (new Node (this, map.getNeighbor (coordinates, Direction.East)));
            ns.add (new Node (this, map.getNeighbor (coordinates, Direction.South)));
            ns.add (new Node (this, map.getNeighbor (coordinates, Direction.West)));
            return ns;
        }

        public int hashCode ()
        {
            return Integer.parseInt ("" + coordinates.x + coordinates.y);
        }

        public boolean equals (Object obj)
        {
            if (obj == null) {
                return false;
            }

            if (obj == this) {
                return true;
            }

            if (obj.getClass () != getClass ()) {
                return false;
            }

            Node n = (Node) obj;
            return n.getCoordinates ().x == coordinates.x
                   && n.getCoordinates ().y == coordinates.y;
        }
    }
}
