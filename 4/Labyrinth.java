import java.awt.Point;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Comparator;

public class Labyrinth
{
    private Map map;
    private Point player;
    private Point finish;

    public Labyrinth (Map m, Point p, Point f)
    {
        map = m;
        player = p;
        finish = f;
    }

    public Point getPlayer ()
    {
        return player;
    }

    public Point getFinish ()
    {
        return finish;
    }

    public Map getMap ()
    {
        return map;
    }

    public boolean move (Direction dir)
    {
        Point newPos = Map.getNeighbor (player, dir);

        if (map.isAccessible (newPos)) {
            player = newPos;
            return true;
        }

        return false;
    }
}
