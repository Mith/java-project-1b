import java.util.Timer;
import java.util.TimerTask;
import java.util.ArrayList;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class LabyrinthGui extends JFrame
{
    private JPanel field;
    private Labyrinth labyrinth;
    private PathFinder pathfinder;
    private MazeGenerator generator;
    private Timer clock;
    private ArrayList<Point> shortestPath;

    // functie om spell aan te maken
    public static void main (String args[])
    {
        LabyrinthGui labGui = new LabyrinthGui();
    }

    //het doolhof
    //spel constructor
    public LabyrinthGui()
    {
        generator = new MazeGenerator (30, 30);
        Map map = new Map (generator.generate ());
        Point player = new Point (1, 1);
        Point finish = new Point (29, 29);
        labyrinth = new Labyrinth (map, player, finish);
        pathfinder = new PathFinder (map);
        // initialize gui
        getContentPane().setBackground (Color.GRAY);
        setLayout (new BorderLayout());
        //veld aanmaken waarin getekend wordt
        field = new JPanel();
        this.setFocusable (false);
        field.setFocusable (true);
        add (field, BorderLayout.CENTER);
        //zorgen dat je kan lopen
        Action upAction = new UpAction();
        Action downAction = new DownAction();
        Action leftAction = new LeftAction();
        Action rightAction = new RightAction();
        field.getInputMap().put (KeyStroke.getKeyStroke ("UP"), "up");
        field.getInputMap().put (KeyStroke.getKeyStroke ("DOWN"), "down");
        field.getInputMap().put (KeyStroke.getKeyStroke ("LEFT"), "left");
        field.getInputMap().put (KeyStroke.getKeyStroke ("RIGHT"), "right");
        field.getActionMap().put ("up", upAction);
        field.getActionMap().put ("down", downAction);
        field.getActionMap().put ("left", leftAction);
        field.getActionMap().put ("right", rightAction);
        setSize (800, 600);
        setVisible (true);
        addComponentListener (new ComponentAdapter () {
            public void componentResized (ComponentEvent e) {
                renderMap ();
                renderActors ();
            }
        });
        clock = new Timer();
        clock.schedule (new TimerTask() {
            public void run () {
                renderMap ();
                renderActors ();
            }
        }, 100);
    }

    private void render ()
    {
        renderMap ();
        renderActors ();
    }

    private void renderMap ()
    {
        Map map = labyrinth.getMap ();
        Graphics g = field.getGraphics ();

        for (int y = 0; y < map.getMapHeight (); y++) {
            for (int x = 0; x < map.getMapWidth (y); x++) {
                switch (map.getMapValue (x, y)) {
                case 1:
                    g.setColor (Color.GRAY);
                    break;

                case 0:
                    g.setColor (Color.BLACK);
                    break;

                default:
                    break;
                }

                g.fillRect (x * 10, y * 10, 10, 10);
            }
        }
    }

    private void renderActors ()
    {
        Graphics g = field.getGraphics ();
        g.setColor (Color.RED);
        //speler tekenen
        g.fillOval (10 * labyrinth.getPlayer().x, 10 * labyrinth.getPlayer().y, 10, 10);
        // finish
        g.setColor (Color.CYAN);
        g.fillRect (10 * labyrinth.getFinish().x, 10 * labyrinth.getFinish().y, 10, 10);

        // shortestpath
        if (shortestPath != null) {
            g.setColor (Color.BLUE);

            for (Point p : shortestPath) {
                g.fillOval (10 * p.x + 2, 10 * p.y + 2, 5, 5);
            }
        }
    }

    //classes zodat het programma weet wat hij moet doen bij pijltjes toetsen
    class UpAction extends AbstractAction
    {
        public void actionPerformed (ActionEvent e)
        {
            labyrinth.move (Direction.North);
            shortestPath = pathfinder.findPath (labyrinth.getPlayer(), labyrinth.getFinish());
            render ();
        }
    }

    class DownAction extends AbstractAction
    {
        public void actionPerformed (ActionEvent e)
        {
            labyrinth.move (Direction.South);
            shortestPath = pathfinder.findPath (labyrinth.getPlayer(), labyrinth.getFinish());
            render ();
        }
    }

    class LeftAction extends AbstractAction
    {
        public void actionPerformed (ActionEvent e)
        {
            labyrinth.move (Direction.West);
            shortestPath = pathfinder.findPath (labyrinth.getPlayer(), labyrinth.getFinish());
            render ();
        }
    }

    class RightAction extends AbstractAction
    {
        public void actionPerformed (ActionEvent e)
        {
            labyrinth.move (Direction.East);
            shortestPath = pathfinder.findPath (labyrinth.getPlayer(), labyrinth.getFinish());
            render ();
        }
    }
}
